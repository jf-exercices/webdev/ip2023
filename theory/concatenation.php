<?php

$a = "Hello, ";
$b = "comment allez‑vous?";
$c = "Jean-François";
$c = $a . $c . ' ' . $b;
echo $c;

// Attention à l'ordre des instructions
$a = "Salut, ";
var_dump($c);
var_dump($a);