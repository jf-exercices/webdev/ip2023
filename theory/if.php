<?php
header("Content-Type: text/plain");

$a = '';

// la variable $a sera égal à false (booléen) :
echo '1e condition SI = ';
if ($a == false) {
    // si la condition du test renvoie VRAI (true), alors les instructions du bloc délimité par les accolades {}
    // après les parenthèses du test sera exécuté.
    echo 'la variable $a est égale au booléen false' . PHP_EOL;
}
// équivalence de la 1e condition avec une syntaxe différente, l'opérateur de négation
echo '2e condition SI = ';
if (!$a) {
    echo 'la variable $a est égale au booléen false' . PHP_EOL;
}
// Combinaison de plusieurs comparaisons dans un seul test
// la variable $a sera égal à false (booléen) si elle a pour valeur : false OU 0 OU null OU ''
echo '3e condition SI (IF) = ';
// ce test est équivalent aux deux premiers
if ($a == false || $a == 0 || $a == null || $a == '') {
    echo 'la variable $a est égale au booléen false' . PHP_EOL;
}

echo '1e condition SI ... SINON (IF ... ELSE) = ';
// Dans le cas d'une condition IF ... ELSE, l'exécution des blocs est un OU EXCLUSIF (XOR)
// Un seul bloc d'instructions sera exécuté, jamais les deux
if ($a == true) {
    echo 'la variable $a est égale à true' . PHP_EOL;
} else {
    // si la condition du test renvoie FAUX (false), alors les instructions du bloc délimité par les accolades {}
    // après le ELSE sera exécuté.
    echo 'la variable $a n\'est pas égale à true' . PHP_EOL;
}
// Les instructions en dehors des accolades du test seront exécutées peu importe le résultat du test
echo 'la variable $a a été évaluée';