<?php

// declaration
$a = 6;
$b = 2;
$c = null;

// logical instructions
$c = $a + $b;

// output
echo $c;
echo $a - $b;

var_dump($c, $b);

// operators priority
$a = $c - $b * 2;

echo $a * $b;
echo $a / $b;
echo $a % $b;
echo $a ** $b;

// output separated
//echo $a + $b . ' ';
//echo $a - $b . ' ';
//echo $a * $b . ' ';
//echo $a / $b . ' ';
//echo $a % $b . ' ';
//echo $a ** $b . ' ';