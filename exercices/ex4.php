<?php

/**
 * Écrivez un algorithme permettant d’évaluer une note sur 20 d’un étudiant.
 * S’il a une note de 20, affichez “parfait !”.
 * S’il a une note comprise entre 18 inclus et 20 non inclus, affichez “excellent”.
 * S’il a une note comprise entre 16 inclus et 18 non inclus, affichez “très bien”.
 * S’il a une note comprise entre 14 inclus et 16 non inclus, affichez “bien”.
 * S’il a une note comprise entre 12 inclus et 14 non inclus, affichez “satisfaisant”.
 * S’il a une note supérieure ou égale à 10, affichez “réussite”.
 * S’il a une note inférieure à 10, affichez “échec”.
 */

$note = null;

if (is_numeric($note) && $note <= 20) {
    if ($note == 20) {
        echo 'parfait!';
    } elseif ($note >= 18) {
        echo 'excellent';
    } elseif ($note >= 16) {
        echo 'très bien';
    } elseif ($note >= 14) {
        echo 'bien';
    } elseif ($note >= 12) {
        echo 'satisfaisant';
    } elseif ($note >= 10) {
        echo 'réussite';
    } else {
        echo 'échec';
    }
} else {
    echo 'Note incorrecte. La valeur doit être une note sur 20';
}
