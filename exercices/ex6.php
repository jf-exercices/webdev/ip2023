<?php

/**
 * Choisissez un schéma logique approprié et écrivez un algorithme permettant d’afficher
 * tous les nombres entiers entre 0 et 100.
 */

$nbr = 0;

do {
    echo $nbr;
    $nbr ++;
} while ($nbr <= 100);

echo PHP_EOL;

$nbr = 0;

while ($nbr <= 100) {
    echo $nbr;
    $nbr ++;
}

echo PHP_EOL;

for ($nbr = 0; $nbr <= 100; $nbr++) {
    echo $nbr;
}