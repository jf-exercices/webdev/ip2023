<?php

/**
 * 1. Créez plusieurs variables qui devront contenir :
 *
 * - le code postal d’une ville
 * - la réponse à une question “vrai ou faux”
 * - la quantité d’un produit en stock
 * - le statut de connexion d’un utilisateur (connecté ou non connecté)
 * - une liste des jours de la semaine
 * - l’identifiant d’un utilisateur
 * - le prix d’un article
 * - l’adresse email d’un utilisateur
 * - les notes d’un étudiant
 *
 * 2. Écrivez un bloc de commentaire introduisant vos variables et une ligne de commentaire pour au moins deux d’entre elles.
 *
 * 3. Affichez les différentes variables déclarées dans votre script.
 */

$codePostal = 5000; // int | une châine de caractères (string) pourrait également convenir en dehors de la Belgique.
$postalCode = '4753S'; // string
$response = false; // boolean (true or false)
$quantity = 0; // int
$status = false; // boolean
$status2 = ['connecté', 'non connecté']; // array of strings
$days = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche']; // array of strings
$id = 1; // UNIQUE int | un identifiant doit toujours être unique.
$price = 0.00; // float
$email = 'test@test.com'; // string
$notes = [16, 20, 12, 14, 13, 17]; // array of integers

// fonction permettant de définir du contenu texte au lieu du HTML
// Cela permet d'utiliser la constante native PHP_EOL (ou "\r\n" sous Windows)
// Attention que la fonction header() DOIT être appelée AVANT TOUT AFFICHAGE (echo, print, HTML, etc...)
header("Content-Type: text/plain");

echo $codePostal . PHP_EOL;
echo $postalCode . PHP_EOL;

// la fonction native var_dump() permet d'afficher le contenu d'une variable, mais également son type et tous les éléments d'un tableau.
var_dump($codePostal);
var_dump($days);

// la fonction get_defined_vars() permet d'afficher TOUTES les variables du script courant et leurs valeurs.
var_dump(get_defined_vars());
