<?php

/**
 * Écrivez un algorithme permettant d’afficher le statut de jour férié (férié ou non) d’une date fixe sur base de quatre données : le numéro du jour dans le mois, le mois, l’année et le nom du jour de la semaine.
 * Si le jour férié tombe un samedi ou un dimanche, il ne doit pas être considéré comme un jour férié.
 *
 * Les jours fériés à date fixe sont :
 *
 * Le 1er janvier (jour de l'An)
 * Le 1er mai (fête du Travail)
 * Le 21 juillet (fête nationale)
 * Le 15 août (Assomption)
 * Le 1er novembre (Toussaint)
 * Le 11 novembre (Armistice de 1918)
 * Le 25 décembre (Noël)
 *
 * L’anniversaire de votre meilleur(e) ami(e) tombe le 29 février. Ajoutez à votre algorithme la possibilité de préciser si, cette année-là, il pourra fêter son anniversaire le 29 février ou non.
 * Une année est considérée comme bissextile si au moins une de ces conditions est vraie :
 *
 * L'année est divisible par 4 sans être divisible par 100 (cas des années qui ne sont pas des multiples de 100)
 * L'année est divisible par 400 (cas des année multiples de 100).
 */


// Valeurs d'entrée
$monthDay = 29;
$month = 2;
$year = 2024;
$weekDay = 'mardi';

// Le statut d'un jour férié (oui ou non) doit donc être un booléen. Il en va de même pour l'anniversaire le 29 février ou non.
$holiday = false;
$birthday = false;

// Regroupement des conditions de notre premier test en combinant les opérateurs logiques et de comparaison
if ($weekDay != 'samedi' &&
    $weekDay != 'dimanche' &&
    (
        ($monthDay == 1 &&
            (
                ($month == 1) ||
                ($month == 5) ||
                ($month == 11)
            )
        ) ||
        ($monthDay == 11 && $month == 11) ||
        ($monthDay == 15 && $month == 8) ||
        ($monthDay == 25 && $month == 12)
    )
) {
    $holiday = true;
}

// Priorité des opérateurs : pas de parenthèses dans ce cas, car les opérateurs logiques sont prioritaires aux opérateurs de comparaison
// Opérateur de comparaison stricte : Les valeurs d'entrée peuvent être int, string ou float sans décimale; par contre le reste de la division ne peut être false
if ($monthDay == 29 && $month == 2 &&
        (($year % 4 === 0 && $year % 100 !== 0) ||
            $year % 400 === 0
        )
    ) {
    $birthday = true;
}


// Affichage du résultat (valeur de sortie)
if ($holiday) {
    echo 'C\'est un jour férié !' . PHP_EOL;
} else {
    echo 'Ce n\'est pas un jour férié !' . PHP_EOL;
}
if ($birthday) {
    echo 'Tu pourras fêter ton anniversaire ce jour là !';
} else {
    echo 'Tu ne pourras pas fêter ton anniversaire ce jour là !';
}


/**
 * Fonction permettant de renvoyer sous forme d'un booléen (true|false) le fait qu'une date soit un jour férié ou non
 *
 * @param string $weekDay
 * @param int $monthDay
 * @param int $month
 * @return bool
 */
function isHoliday(string $weekDay, int $monthDay, int $month): bool
{
    $holiday = false;
    if ($weekDay != 'samedi' &&
        $weekDay != 'dimanche' &&
        (
            ($monthDay == 1 &&
                (
                    ($month == 1) ||
                    ($month == 5) ||
                    ($month == 11)
                )
            ) ||
            ($monthDay == 11 && $month == 11) ||
            ($monthDay == 15 && $month == 8) ||
            ($monthDay == 25 && $month == 12)
        )
    ) {
        $holiday = true;
    }
    return $holiday;
}