<?php

/**
 * Créez un script permettant d'envoyer un message type à partir des trois données suivantes :
 * - En premier lieu, le nom du destinataire, précédé du mot "Bonjour" et terminé par une virgule.
 * - En deuxième lieu, le contenu du message, séparé par un espace avant et après le texte.
 * - Enfin, le nom de l'expéditeur, précédé de la formule de politesse "Bien à vous" suivi d'une virgule, à la fin du message.
 *
 * Le résultat devra donner ceci :
 * Bonjour Destinataire, voici un exemple de message. Bien à vous, Expéditeur
 */


//$recipient = 'Patrick';
//$sender = 'JF';
//$message = 'Coucou';
//echo 'Bonjour ' . $recipient . '. ' . $message . '. Bien  à vous, ' . $sender;
//$recipient = 'Dominique';
//$sender = 'Michel';
//$message = 'Adieu!';
//echo 'Bonjour ' . $recipient . '. ' . $message . '. Bien  à vous, ' . $sender;
//$recipient = 'Mohamed';
//$sender = 'Adeline';
//$message = 'Hello';
//echo 'Bonjour ' . $recipient . '. ' . $message . '. Bien  à vous, ' . $sender;


// PS : n'utilisez PAS de balises HTML
header("Content-Type: text/plain");

function sendMessage(string $recipient, string $message, string $sender) : string
{
    return PHP_EOL . 'Bonjour ' . $recipient . ',' . PHP_EOL . ' ' . $message . ' ' . PHP_EOL . 'Bien à vous, ' . $sender . PHP_EOL;
}

echo sendMessage('Monsieur le Directeur', 'Comment allez-vous ?', 'JF Vanass');
echo sendMessage('Patrick', 'Coucou!', 'Michel');
echo sendMessage('Madame', 'Veuillez confirmer votre inscription à notre site.', 'Le service client');

// Envoyer un message à 1000 utilisateurs identifiés par un identifiant numérique unique
$recipients = range(1, 1000); // fonction native PHP générant un ARRAY

foreach ($recipients AS $recipient) {
    echo sendMessage($recipient, 'Coucou', 'JF Vanass');
}