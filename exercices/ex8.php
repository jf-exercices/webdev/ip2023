<?php

/**
 * Rappelez-vous d’un exercice précédent :
 * “Choisissez un schéma logique approprié et écrivez un algorithme permettant de créer une liste de valeurs aléatoires comprises entre 0 et 100.
 * Cette liste contiendra un nombre aléatoire de valeurs comprises entre 5 et 20.”
 *
 * Pour chacune de ces valeurs, affichez lors du dernier passage le nombre de valeurs totales ainsi que le nombre de valeurs inférieures,
 * et supérieures ou égales à 50, ainsi que le % du total pour chacun de ces deux résultats.
 * Écrivez un algorithme sans utiliser de boucle de parcours et un autre avec boucle de parcours.
 */

$max = rand(5, 20);

// without foreach
$stat = [
    '-50' => 0,
    '+=50' => 0,
];
$values = [];
for ($x = 1; $x <= $max; $x++) {
    $rand = rand(0, 100);
    $values[] = $rand;
    if ($rand >= 50) {
        $stat['+=50']++;
    } else {
        $stat['-50']++;
    }
    if ($x == $max) {
        echo 'Le nombre de valeurs totales : ' . $max . PHP_EOL;
        echo 'Le nombre de valeurs inférieures à 50 : ' . $stat['-50'] . ' (' . ($stat['-50'] / $max * 100) . '%)' . PHP_EOL;
        echo 'Le nombre de valeurs supérieures ou égales à 50 : ' . $stat['+=50']. ' (' . ($stat['+=50'] / $max * 100) . '%)' . PHP_EOL;
    }
}

// foreach option
$stat = [
    '-50' => 0,
    '+=50' => 0,
];
$values = [];
for ($x = 1; $x <= $max; $x++) {
    $values[] = rand(0, 100);
}
foreach ($values as $value) {
    if ($value >= 50) {
        $stat['+=50']++;
    } else {
        $stat['-50']++;
    }
}
echo 'Le nombre de valeurs totales : ' . $max . PHP_EOL;
echo 'Le nombre de valeurs inférieures à 50 : ' . $stat['-50'] . ' (' . ($stat['-50'] / $max * 100) . '%)' . PHP_EOL;
echo 'Le nombre de valeurs supérieures ou égales à 50 : ' . $stat['+=50']. ' (' . ($stat['+=50'] / $max * 100) . '%)' . PHP_EOL;