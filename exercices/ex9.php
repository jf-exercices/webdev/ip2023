<?php

/**
 * Choisissez un schéma logique approprié et écrivez un algorithme permettant d’afficher une liste de combinaisons de coupures de 10, 20 et 50€ disponibles à partir d’un montant prédéterminé.
 */

// amount in euros
$amount = 2760;

// bank denominations
$bank = [
    10 => 100,
    20 => 50,
    50 => 20
];

// denominations output
$denominations = [
    50 => 0,
    20 => 0,
    10 => 0,
];

// logic
$rest = $amount;

while ($rest > 0) {

    // code sans l'utilisation d'une fonction : ne respecte pas le principe DRY
//    $fifty = floor($rest / 50);
//    if ($bank[50] >= $fifty) {
//      $denominations[50] += $fifty;
//      $bank[50] -= $fifty;
//      $rest -= ($fifty * 50);
//    }
//    $twenty = floor($rest / 20);
//    if ($bank[20] >= $twenty) {
//      $denominations[20] += $twenty;
//      $bank[20] -= $twenty;
//      $rest -= ($twenty * 20);
//    }
//    $ten = floor($rest / 10);
//    if ($bank[10] >= $ten) {
//      $denominations[10] += $ten;
//      $bank[10] -= $ten;
//      $rest -= ($ten * 10);
//    }

    foreach ($denominations as $key => $value) {
        $rest -= getDenominationQty($key, $rest);
    }
}

// output
echo 'Pour un montant de : ' . $amount . PHP_EOL;
foreach ($denominations as $key => $value) {
    echo $denominations[$key] . ' coupures de ' . $key . ' euros' . PHP_EOL;
}

/**
 * Fonction utilisateur PHP
 * getDenomination est le nom de la fonction
 * Cette fonction possède 2 paramètres (qui sont les VALEURS d'ENTREE du bloc d'instruction de la fonction)
 * Ces paramètres sont des variables qui n'existent que dans le bloc d'instruction de la fonction. On parle alors de PORTEE des variables.
 * Les paramètres peuvent avoir un TYPE strict
 * La fonction possède également une VALEUR de RETOUR qui peut être de n'importe quel type (type VOID compris)
 *
 * @param int $denomination
 * @param int $amount
 * @return int
 */
function getDenominationQty(int $denomination, int $amount): int {
    // variables dont la portée vaut pour les instructions à l'intérieur ET à l'extérieur de la fonction
    global $denominations;
    global $bank;

    // La variable $qty n'existe QUE dans la fonction. Sa portée est limitée à ce bloc d'instructions
    $qty = floor($amount / $denomination);
    // on vérifie qu'il reste suffisamment de billets
    if ($bank[$denomination] >= $qty) {
        // on incrémente les variables globales (existantes HORS de la fonction)
        $denominations[$denomination] += $qty;
        $bank[$denomination] -= $qty;
    } else {
        $qty = 0;
    }
    // valeur de RETOUR de la fonction (doit correspondre avec le TYPE précisé dans la définition après les : )
    return $qty * $denomination;
}