<?php

/**
 * Choisissez un schéma logique, parmi ceux vus au cours, permettant de résoudre l'algorithme adapté à l'énoncé suivant :
 * Créez et un script permettant de tester la valeur d’une variable numérique.
 * Si la valeur est strictement supérieure à zéro, affichez “positif”.
 * Si elle est strictement inférieure à zéro, affichez “négatif”, sinon affichez “zéro”.
 */


$a = null;

// Schéma logique SI ... SINON SI ... SINON
if ($a > 0) {
    $result = 'positif';
} elseif ($a < 0) {
    $result = 'négatif';
} else {
    $result = 'zéro';
}
echo $result;
