<?php

// associative array
$notes = [ 'Dominique' => 17.5, 'Patrick' => 16, 'Jean' => 10, 'Leïla' => 7, 'Christophe' => 12, 'Eric'
=> 2, 'François' => 10, 'Vladimir' => 0, 'Loïc' => 13, 'Sophie' => 14, 'Louka' => 12, 'Emmanuel'
=> 8, 'Marie' => 15, 'Hugo' => 18 ];


// boucle de parcours foreach où $key représente la clé associée à $value qui représente chaque valeur
foreach ($notes AS $key => $value) {
    // si l'élément du tableau n'est pas numérique et est strictement différent de 0
    if (!is_numeric($value) && $value !== 0) {
        echo "La note de " . $key . " n’ a pas été encodée" . PHP_EOL;
    } elseif ($value >= 10)  {
        echo $key . PHP_EOL;
    }
}